Изменение статуса заказа и отправка на сайт
===
1. Идем Настройки-Коммуникации-Триггеры
2. Создаем триггер webhook с действием выполнить Http запрос.  POST. Передавать параметры в теле запроса
3. Условия срабатывания триггера  ```changeSet.hasChangedField("status") and changeSet.getNewValue("status").getCode() in ["sh-send", "cancel-other"]```
4. Передаваемые параметры status = ```{{ order.getStatus().getCode()  }}```   order = ```{{ order.getExternalId() }}```
5. ```
<?php
define('MODX_API_MODE', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/index.php');

$modx = new modX();
$modx->initialize('web');
$modx->getService('error','error.modError', '', '');
$modx->setLogLevel(modX::LOG_LEVEL_ERROR);


if($_POST['status'] && $_POST['order']){
	$status = trim( filter_input(INPUT_POST,'status',  FILTER_SANITIZE_STRING) );
	$order = trim( filter_input(INPUT_POST,'order',  FILTER_SANITIZE_STRING) );

	$new_status = 0;
	switch($status){
		case 'sh-send':
			$new_status = 3;
			break;
		case 'cancel-other':
			$new_status = 4;
			break;
	}

	$order = $modx->getObject('msOrder', array('num' => $order));
	if($order && $new_status > 0){
		$order->set('status', $new_status);
		$order->save();
	}
}

//$modx->log(1, '[RetailCRM webHook] '.print_r($_POST, true));
```