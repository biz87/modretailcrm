<?php
if (!$modx->getService('modretailcrm','modRetailCrm', MODX_CORE_PATH.'components/modretailcrm/model/modretailcrm/')) {  
    $modx->log(1, '[ModRetailCrm] - Not found class RetailCrm');
    return;
}

$pdo = $modx->getService('pdoFetch');

$site = $modx->getOption('modretailcrm_siteCode');
$apiKey = $modx->getOption('modretailcrm_apiKey');
$crmUrl = $modx->getOption('modretailcrm_url');

$modRetailCrm = new modRetailCrm($modx, $apiKey, $crmUrl, $site);

switch ($modx->event->name) {
    case 'OnUserSave':
        if ($mode == modSystemEvent::MODE_NEW) {
            if ($modx->context->key != 'mgr' ) {
                if ($profile = $modx->getObject('modUserProfile', $user->get('id'))) {
                    $customer = array();
                    $customer['externalId'] =  $user->get('id');
                    $customer['firstName'] = $profile->fullname;
                    $customer['email'] = $profile->email;
                    if(!empty($profile->phone)){
                        $customer['phones'][]['number'] = $profile->phone;
                    }
                    
                    $response = $modRetailCrm->request->customersCreate($customer, $site);  
                    
                }
                
            }
        }
        break;
    case 'msOnCreateOrder':
        $order = $msOrder->toArray();
        $order['address'] = $pdo->getArray('msOrderAddress', array('id' => $order['id']), array('sortby' => 'id'));
        $order['delivery'] = $pdo->getArray('msDelivery', array('id' => $order['delivery']), array('sortby' => 'id'));
        $order['payment'] = $pdo->getArray('msPayment', array('id' => $order['payment']), array('sortby' => 'id'));
        $order['profile'] = $pdo->getArray('modUserProfile', array('internalKey' => $order['user_id']), array('sortby' => 'id'));
        $order['products'] = $pdo->getCollection('msOrderProduct', array('order_id' => $order['id']), array('sortby' => 'id'));        
        
        $orderData = array();
        //Проверяю наличие пользователя в базе CRM
        $user_response = $modRetailCrm->request->customersGet($order['user_id'], 'externalId', $site); 
        if($user_response->getStatusCode() == 404){
            $customer_profile = $pdo->getArray('modUserProfile', array('internalKey' => $order['user_id']));
            $customer = array();
            $customer['externalId'] =  $order['user_id'];
            $customer['firstName'] = $customer_profile['fullname'];
            $customer['email'] = $customer_profile['email'];
            if(!empty($customer_profile['phone'])){
                $customer['phones'][]['number'] = $customer_profile['phone'];
            }
            $response = $modRetailCrm->request->customersCreate($customer, $site);  
        }
        $orderData['customer']['externalId'] = $order['user_id'];
        $orderData['externalId'] = $order['num'];
        $orderData['firstName'] = !empty($order['address']['receiver']) ? $order['address']['receiver'] : $order['profile']['fullname'];
        $tmpName = explode(' ', $orderData['firstName']);
        if(count($tmpName == 3)){
            $orderData['lastName'] = $tmpName[0];
            $orderData['firstName'] = $tmpName[1];
            $orderData['patronymic'] = $tmpName[2];
        }
        $orderData['phone'] = !empty($order['address']['phone']) ? $order['address']['phone'] : $order['profile']['phone'];
        $orderData['email'] = $order['profile']['email'];
     
        foreach ($order['products'] as $key=>$product) {
            $orderData['items'][$key]['initialPrice'] = $product['price'];
            $orderData['items'][$key]['purchasePrice'] = $product['price'];
            $orderData['items'][$key]['productName'] = $product['name'];
            $orderData['items'][$key]['quantity'] = $product['count'];
            $orderData['items'][$key]['offer']['externalId'] = $product['id'];
            foreach($product['options'] as $k=>$v){
                $orderData['items'][$key]['properties'][] = array('name' => $k, 'value' => $v); 
            }
		}
		if($order['weight']> 0){
		    $orderData['weight'] = $order['weight'] * 1000;
		}
		
		$fields = array(
            'index' => 'Индекс', 
            'country' => 'Страна', 
            'region' => 'Регион', 
            'city' => 'Город', 
            'metro' => 'Метро', 
            'street' => 'Улица', 
            'building' => 'Дом', 
            'room' => 'Квартира\офис'
        );
        $address = '';
        foreach($fields as $field=>$comment){
            if(!empty($order['address'][$field])){
                $address .= $comment.':'.$order['address'][$field].' 
                ';
                if($field == 'room'){
                    $orderData['delivery']['address']['flat'] = $order['address'][$field];
                }else{
                    $orderData['delivery']['address'][$field] = $order['address'][$field];
                }
                
            }
        }
        
        $orderData['delivery']['address']['text'] = $address;
        //$order['properties']['msshiptor']['courier']['method']['courier']
        
        
        switch($order['delivery']['id']){
            //Самовывоз
            case 2:
                if(!empty($order['properties']['msshiptor']['courier']['method']['courier'])){
                    $orderData['delivery']['code'] = 'pickpoint-2';
                    $orderData['delivery']['address']['notes'] = $order['properties']['msshiptor']['order_info'];
                    
                }else{
                    $orderData['delivery']['code'] = 'self-delivery';
                }
                
                break;
            //Почта россии
            case 3:
                $orderData['delivery']['code'] = 'russian-post';
                $orderData['delivery']['address']['notes'] = $order['properties']['msshiptor']['order_info'];
                break;
            //Курьер
            case 4:
                if(!empty($order['properties']['msshiptor']['courier']['method']['courier'])){
                    $shiptorMethod = $order['properties']['msshiptor']['courier']['method']['courier'];
                    switch($shiptorMethod){
                        case 'iml':
                            $orderData['delivery']['code'] = 'iml-2';
                            break;
                        case 'dpd':
                            $orderData['delivery']['code'] = 'dpd-2';
                            break;
                        case 'shiptor-one-day':
                            $orderData['delivery']['code'] = 'shiptor-one-day-2';
                            break;
                        case 'cdek':
                            $orderData['delivery']['code'] = 'cdek-2';
                            break;
                        case 'shiptor':
                            $orderData['delivery']['code'] = 'shiptor-2';
                            break;
                            default:
                                 $orderData['delivery']['code'] = $shiptorMethod;
                    }
               
                    $orderData['delivery']['address']['notes'] = $order['properties']['msshiptor']['order_info'];
                    
                }else{
                    $orderData['delivery']['code'] = 'courier';
                }
                
                break;
            //За пределы РФ
            case 5:
                $orderData['delivery']['code'] = 'other-country';
                break;
        }
       
        $orderData['customerComment'] = $order['address']['comment'] . '.
        Информация по доставке:'. $orderData['delivery']['address']['notes'];
        $orderData['delivery']['cost'] = $order['properties']['msshiptor']['courier.cost.total.calc'];
        switch($order['payment']['id']){
            //Наличные
            case 1:
                $orderData['payments'][0]['type'] = 'cash';
                break;
            //Онлайн
            case 4:
                $orderData['payments'][0]['type'] = 'online';
                break;
        }
        
        $response = $modRetailCrm->request->ordersCreate($orderData, $site);  
        $modx->log(1, '[modRetailCRM] resplonse'.print_r($response, true));
        break;
}